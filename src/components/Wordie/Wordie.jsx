import { createSignal, createEffect, For, Switch, Match, onMount, Show } from 'solid-js'
import { createStore } from 'solid-js/store'

import { WORDS } from '../../data/words'
import { ANSWERS } from '../../data/answers'

import styles from "./Wordie.module.css"
import WordGrid from "./WordGrid/WordGrid"
import Keyboard from "./Keyboard/Keyboard"
import InfoPopup from "./InfoPopup/InfoPopup"
import StatsPopup from "./StatsPopup/StatsPopup"

const matrix = [
  ['00', '01', '02', '03', '04'],
  ['10', '11', '12', '13', '14'],
  ['20', '21', '22', '23', '24'],
  ['30', '31', '32', '33', '34'],
  ['40', '41', '42', '43', '44'],
  ['50', '51', '52', '53', '54']
]

const keyRows = [
  ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
  ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
  ['Enter', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'Backspace']
]

const currentDate = new Date()
//const currentDate = new Date(2022, 3, 22) // this date is for development purposes
if (localStorage.getItem('startDate') === null) {
  localStorage.setItem('startDate', Date.UTC(2022, 4, 19))
  localStorage.setItem('currentDate', Date.UTC(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate()))
  localStorage.setItem('rowIndex', 0)
  localStorage.setItem('matrixStore', JSON.stringify(buildMatrixStore(matrix)))
  localStorage.setItem('keyStore', JSON.stringify(buildKeyStore(keyRows)))
  localStorage.setItem('guessHistory', JSON.stringify({
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    'X': 0
  }))
  localStorage.setItem('maxStreak', 0)
  localStorage.setItem('currentStreak', 0)
}

// reset for a new day
if (parseInt(localStorage.getItem('currentDate'))
  - (Date.UTC(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate()))
  !== 0) {
  localStorage.setItem('currentDate', Date.UTC(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate()))
  localStorage.setItem('rowIndex', 0)
  localStorage.setItem('matrixStore', JSON.stringify(buildMatrixStore(matrix)))
  localStorage.setItem('keyStore', JSON.stringify(buildKeyStore(keyRows)))
}

const timeout = 350
const [guess, setGuess] = createSignal([])
const [stepIndex, setStepIndex] = createSignal(0)
const [currentStreak, setCurrentStreak] = createSignal(
  localStorage.getItem('currentStreak')
)
const [maxStreak, setMaxStreak] = createSignal(
  localStorage.getItem('maxStreak')
)
const [rowIndex, setRowIndex] = 
  createSignal(parseInt(localStorage.getItem('rowIndex')))

const [isNotValidWord, setIsNotValidWord] = createSignal(false)
const [isComplete, setIsComplete] = createSignal(false)
const [failed, setFailed] = createSignal(false)
const [viewStats, setViewStats] = createSignal(false)
const [numOfGuesses, setNumOfGuesses] = createSignal(0)
const [guessHistory, setGuessHistory] = createSignal(JSON.parse(
  localStorage.getItem('guessHistory'))
)

const [keyStore, setKeyStore] = createStore({ 
  keys: JSON.parse(localStorage.getItem('keyStore'))
})
const [matrixStore, setMatrixStore] = createStore({
  matrix: JSON.parse(localStorage.getItem('matrixStore')) 
})

const index = Object.keys(ANSWERS)
const answer = index[dateToAnswerIndex()]
ANSWERS[answer] = true
console.log(answer)

function buildMatrixStore(matrix) {
  let matrixObj = {}
  matrix.forEach((row, i) => {
    row.forEach((id, j) => {
      matrixObj[`${i}${j}`] = {
        id: id,
        gridBoxColor: '',
        filled: false,
        guessedLetter: '',
        backAnimation: false,
        flipAnimation: false,
        correctAnimation: false
      }
    })
  })
  return matrixObj
}

function buildKeyStore(keyRows) {
  let keyRowObj = {}
  keyRows.forEach(row => {
    row.forEach(letter => {
      keyRowObj[letter] = {
        id: letter,
        keyColor: ''
      }
    })
  })
  return keyRowObj
}

function dateToAnswerIndex() {
  return (
    (Date.UTC(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate()) 
    - parseInt(localStorage.getItem('startDate'))) 
    / 86400000
  )
}

export default function Wordie() {

  // sets correct answer notification upon failure
  createEffect(() => {
    if (rowIndex() === 6 && !isComplete()) {
      setTimeout(() => {
        setFailed(true)
        setTimeout(() => {
          setFailed(false)
        }, 10000);
      }, (timeout * matrix[0].length) + (timeout * (stepIndex() + 1)));
    }
  })

  // updates guess history on success
  createEffect(() => {
    // when rowIndex = 6 then game is over
    if (rowIndex() === 6 && isComplete()) {
      const a = guessHistory()
      a[numOfGuesses()] = a[numOfGuesses()] + 1
      setGuessHistory(a)
      localStorage.setItem('guessHistory', JSON.stringify(guessHistory()))
    } else if (rowIndex() === 6 && !isComplete()) {
      // updates guess history on failure
      const a = guessHistory() // not sure why a one-liner breaks here
      console.log('adding one');
      a['X'] = a['X'] + 1
      setGuessHistory(a)
      localStorage.setItem('guessHistory', JSON.stringify(guessHistory()))
    }
  })

  createEffect(() => {
    localStorage.setItem('rowIndex', rowIndex())
  })
  createEffect(() => {
    localStorage.setItem('keyStore', JSON.stringify(keyStore.keys))
  })
  createEffect(() => {
    localStorage.setItem('matrixStore', JSON.stringify(matrixStore.matrix))
  })

  document.addEventListener("keyup", (event) => {
    if (event.key === "Backspace" && stepIndex() > 0) {
      setMatrixStore(
        "matrix", 
        `${rowIndex()}${stepIndex() - 1}`, 
        {guessedLetter: "", filled: false, backAnimation: true})
      setStepIndex(stepIndex() - 1)
      setGuess(guess().slice(0, -1))
    }
    else if (event.key === "Enter"
      && rowIndex() < matrix.length
      && stepIndex() === matrix[0].length) {
      // if this is even a word
      if (checkValidWord(guess().join(''))) {
        setNumOfGuesses(numOfGuesses() + 1)
        // calculate diff
        calculateKeyboardDiff(guess())
        // calculate word row diff
        const rowDiff = calculateRowDiff(guess())
        checkGuess(guess(), rowDiff)
        setRowIndex(rowIndex() + 1)
        setStepIndex(0)
        setGuess([])
      } else {
        setIsNotValidWord(true)
        setTimeout(() => {
          setIsNotValidWord(false)
        }, 1500);
      }
    }
    else if (new RegExp(/[A-Z]/i).test(event.key)
      && event.key.length === 1
      && stepIndex() < matrix[0].length
      && rowIndex() < matrix.length) {
      setMatrixStore("matrix", 
        `${rowIndex()}${stepIndex()}`, 
        {guessedLetter: 
            event.key.toUpperCase(), filled: true, backAnimation: false})
      setStepIndex(stepIndex() + 1)
      setGuess([...guess(), event.key.toUpperCase()])
    }
  })

  function checkValidWord(guess) {
    if (typeof WORDS[guess.toLowerCase()] != 'undefined') {
      return true
    }
    return false
  }

  function animateCheck(_letter, step, row, rowDiff) {
    setTimeout(() => {
      setMatrixStore("matrix", `${row}${step}`, {flipAnimation: true})
      setTimeout(() => {
        setMatrixStore("matrix", `${row}${step}`, {gridBoxColor: rowDiff[`${row}${step}`], filled: false})
      }, timeout);
    }, timeout * step + 1);
  }

  function animateCorrect(letter, step, row) {
    setTimeout(() => {
      setMatrixStore("matrix", `${row}${step}`, {correctAnimation: true})
    }, (timeout * matrix[0].length) + (timeout * step + 1));
  }

  function checkGuess(guess, rowDiff) {
    const row = rowIndex()
    guess.forEach((letter, i) => animateCheck(letter, i, row, rowDiff))

    if (ANSWERS[guess.join('')]) {
      guess.forEach((letter, i) => animateCorrect(letter, i, row))
      setIsComplete(true)
      // setting to 6 disables anymore guesses
      setRowIndex(6)
      setCurrentStreak(currentStreak() + 1)
      if (currentStreak() > maxStreak()) {
        setMaxStreak(currentStreak())
        localStorage.setItem('maxStreak', currentStreak())
      }
      localStorage.setItem('currentStreak', currentStreak())
    } else {
      setCurrentStreak(0)
      localStorage.setItem('currentStreak', currentStreak())
    }
  }

  // guess -> array
  // answer -> string
  function calculateKeyboardDiff(guess) {
    setTimeout(() => {
      guess.forEach((letter, i) => {
        if (letter === answer[i]) {
          setKeyStore('keys', letter, 'keyColor', 'green')
        }
      })
      guess.forEach((letter, i) => {
        if (keyStore.keys[letter].keyColor !== 'green'
          && answer.includes(letter)) {
          setKeyStore('keys', letter, 'keyColor', 'yellow')
        }
      })
      guess.forEach((letter, i) => {
        if (keyStore.keys[letter].keyColor === '') {
          setKeyStore('keys', letter, 'keyColor', 'black')
        }
      })
    }, timeout * matrix[0].length);
    
  }

  function getLetterCount(answer) {
    let letterCounts = {}
    answer.forEach((letterKey) => {
      letterCounts[letterKey] = 0
      answer.forEach((currentLetter) => {
        if (currentLetter === letterKey) {
          letterCounts[letterKey] += 1
        }
      })
    })
    return letterCounts
  }

  function calculateRowDiff(guess) {
    let letterCounts = getLetterCount(answer.split(''))
    let diffObj = {}
    guess.forEach((element, i) => {
      diffObj[`${rowIndex()}${i}`] = ''
      if (element === answer[i]) {
        diffObj[`${rowIndex()}${i}`] = 'green'
        letterCounts[element] -= 1
      }
    })

    guess.forEach((element, i) => {
      if (answer.includes(element)
        && diffObj[`${rowIndex()}${i}`] !== 'green'
        && letterCounts[element] > 0) {
        letterCounts[element] -= 1
        diffObj[`${rowIndex()}${i}`] = 'yellow'
      }
      else if (diffObj[`${rowIndex()}${i}`] === '') {
        diffObj[`${rowIndex()}${i}`] = 'black'
      }
    })
    return diffObj;
  }

  function handleViewStats(value, event) {
    setViewStats(value)
  }

  return (
    <>
      <Switch>
        <Match when={isNotValidWord()}>
          <InfoPopup>Not in the word list.</InfoPopup>
        </Match>
        <Match when={failed()}>
          <InfoPopup>Answer was: {answer}</InfoPopup>
        </Match>
        <Match when={viewStats()}>
          <StatsPopup
            setViewStats={setViewStats}
            guessHistory={guessHistory}
            currentStreak={currentStreak}
            maxStreak={maxStreak}
          />
        </Match>
      </Switch>
      <div 
        class={styles.gridWrapper}
        // onKeyUp={handleInput}
        // tabIndex="0"
      >
        <header class={styles.header}>
          <div class={styles.statsButtonContainer}>
            <button onClick={[handleViewStats, true]} class={styles.statsButton}>
              STATS
            </button>
          </div>
          <div class={styles.title}>Wordie</div>
          <div class={styles.titleRight}></div>
        </header>

        <WordGrid
          matrix={matrix}
          matrixStore={matrixStore}
        />
        <Keyboard
          keyRows={keyRows}
          keyStore={keyStore}
        />
      </div>
    </>
  );
}