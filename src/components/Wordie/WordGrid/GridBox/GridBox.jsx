import styles from "./GridBox.module.css"

export default function GridBox(props) {
  const { cell, matrixStore } = props

  function checkBoxColor(cell, color) {
    return matrixStore.matrix[cell].gridBoxColor === color
  }
  function checkIfBoxFilled(cell) {
    return matrixStore.matrix[cell].filled
  }
  function checkBackAnimation(cell) {
    return matrixStore.matrix[cell].backAnimation
  }
  function checkFlipAnimation(cell) {
    return matrixStore.matrix[cell].flipAnimation
  }
  function checkCorrectAnimation(cell) {
    return matrixStore.matrix[cell].correctAnimation
  }
  return (
    <div 
      class={styles.box}
      classList={{
        wiggle: checkIfBoxFilled(cell),
        erase: checkBackAnimation(cell),
        flip: checkFlipAnimation(cell),
        correct: checkCorrectAnimation(cell)
      }}
    >
      <div
        id={cell}
        class={styles.input}
        classList={{
          green: checkBoxColor(cell, 'green'),
          yellow: checkBoxColor(cell, 'yellow'),
          black: checkBoxColor(cell, 'black'),
          filledBorder: checkIfBoxFilled(cell),
        }}
      >
        {matrixStore.matrix[cell].guessedLetter}
      </div>
    </div>
  )
}