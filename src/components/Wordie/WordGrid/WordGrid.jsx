import GridBox from "./GridBox/GridBox"
import styles from "./WordGrid.module.css"

export default function WordGrid(props) {
  const { matrix } = props
  return (
    <section class={styles.wordMatrix}>
      <For each={matrix}>{(row) =>
        <div class={styles.matrixContainer}>
          <For each={row}>{(cell) =>
            <GridBox
              cell={cell}
              matrixStore={props.matrixStore}
            />
          }</For>
        </div>
      }</For>
    </section>
  )
}