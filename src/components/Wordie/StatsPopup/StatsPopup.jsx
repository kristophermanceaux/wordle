import { createEffect } from 'solid-js'
import styles from './StatsPopup.module.css'

export default function StatsPopup (props) {
  const {
    setViewStats, 
    guessHistory,
    currentStreak,
    maxStreak
  } = props
  function handleClose(value, event) {
    setViewStats(value)
  }

  function getGuessDistribution() {
    const totalNumGuesses = Object.values(guessHistory()).reduce((prev, curr) => prev + curr)
    const dist = {}
    Object.keys(guessHistory()).forEach(number => {
      if (guessHistory()[number] === 0) {
        dist[number] = '0%'
      } else {
        const tmp = Math.floor(guessHistory()[number] / totalNumGuesses * 100)
        dist[number] = `${tmp}%`
      }
    })
    return dist
  }

  function getTotalNumberGamesPlayed() {
    return Object.keys(guessHistory())
      .map(current => guessHistory()[current])
      .reduce((prev, current) => prev + current, 0)
  }
  function getTotalNumberGamesWon() {
    return Object.keys(guessHistory())
      .filter((current) => current !== 'X')
      .map(current => guessHistory()[current])
      .reduce((prev, current) => prev + current, 0)
  }

  createEffect(() => {
    Object.keys(guessHistory()).forEach(number => {
      const historyBar = document.getElementById('guesses-' + number)
      historyBar.style.width = getGuessDistribution()[number]
    })
  })

  return (
    <div
      class={styles.modalContainer}
    >
      <div class={styles.modal}>
        <div class={styles.close}>
          <button onClick={[handleClose, false]}>
            X
          </button>
        </div>
        <div class={styles.statisticsContainer}>
          <div className={styles.statSection}>
            <div>{getTotalNumberGamesPlayed()}</div>
            <div>Played</div>  
          </div>
          <div className={styles.statSection}>
            <Show
              when={getTotalNumberGamesPlayed() > 0}
              fallback={<div>0</div>}
            >
              <div>{Math.floor(
                100 * (getTotalNumberGamesWon() / getTotalNumberGamesPlayed())
              )}</div>
            </Show>
            <div>Win %</div>
          </div>
          <div className={styles.statSection}>
            <div>{parseInt(currentStreak())}</div>
            <div>Current</div>
            <div>Streak</div>
          </div>
          <div className={styles.statSection}>
            <div>{parseInt(maxStreak())}</div>
            <div>Max</div>
            <div>Streak</div>
          </div>
        </div>
        <div class={styles.guessDistribution}>
          <h3 class={styles.guessDistributionTitle}>Guess Distribution</h3>
          <For each={Object.keys(guessHistory())}>{(number) =>
            <div class={styles.statContainer}>
              <span class={styles.guessNumber}>{number}</span>
              <span class={styles.historyBar} id={"guesses-" + number}>
                <span>
                  <Show 
                    when={guessHistory()[number] > 0}
                    fallback={''}
                  >
                    {guessHistory()[number]}
                  </Show>
                </span>
              </span>
            </div>
          }</For>
        </div>
      </div>
    </div>  
  )
}