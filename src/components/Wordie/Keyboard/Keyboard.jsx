import Key from "./Key/Key"
import { createStore } from 'solid-js/store'
import styles from "./Keyboard.module.css"

export default function Keyboard(props) {
  const { keyRows, keyStore } = props
  return (
    <section class={styles.keyboard}>
      <For each={keyRows}>{(row) =>
        <div class={styles.keyboardRow}>
          <For each={row}>{(letter) =>
            <Key
              key={letter}
              keyStore={keyStore}
            />
          }</For>
        </div>
      }</For>
    </section>
  )
}