import styles from "./Key.module.css"

export default function Key(props) {
  const { key, keyStore } = props
  function handleButtonClick(key, event) {
    document.dispatchEvent(new KeyboardEvent("keyup", { 'key': key }))
  }

  function checkKeyColor(key, color) {
    return keyStore.keys[key].keyColor === color
  }

  return (
    <Switch fallback={
      <span class={styles.keyButtonContainer}>
        <button
          id={"key-" + key}
          onClick={[handleButtonClick, key]}
          class={styles.keyButton}
          classList={{
            green: checkKeyColor(key, 'green'),
            yellow: checkKeyColor(key, 'yellow'),
            black: checkKeyColor(key, 'black'),
          }}
        >
          {key}
        </button>
      </span>
    }>
      <Match when={key === 'Enter'}>
        <span 
          class={
            `${styles.keyButtonContainer} 
            ${styles.specialButtonContainer}`
          }
        >
          <button 
            id={"key-" + key} 
            onClick={[handleButtonClick, key]}
            class={styles.keyButton}
          >
            ENTER
          </button>
        </span>
      </Match>
      <Match when={key === 'Backspace'}>
        <span
          class={
            `${styles.keyButtonContainer} 
            ${styles.specialButtonContainer}`
          }
        >
          <button
            id={"key-" + key}
            onClick={[handleButtonClick, key]}
            class={styles.keyButton}
          >
            BACK
          </button>
        </span>
      </Match>
    </Switch>
  )
}